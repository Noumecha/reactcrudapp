import React from "react";
import { Nav, Navbar, Container, Row, Col } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import school from "./assets/school.svg";
import "./App.css";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import CreateStudent from "./components/create-student.component";
import EditStudent from "./components/edit-student.component";
import StudentList from "./components/student-list.component";

const App = () => {

  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <Navbar bg="dark" variant="dark">
            <Container>
              <Navbar.Brand>
                <Link to={"/create-student"} className="nav-link">
                  React CRUD App
                </Link>
              </Navbar.Brand>

              <Nav className="justify-content-end">
                <Nav>
                  <Link to={"/create-student"} className="nav-link">
                    Ajouter un étudiant
                  </Link>
                </Nav>
                <Nav>
                  <Link to={"/student-list"} className="nav-link">
                    Liste des étudiants
                  </Link>
                </Nav>
              </Nav>
            </Container>
          </Navbar>
        </header>

        <Container>
          <Row>
            <Col md={6}>
              <div className="wrapper">
                <Routes>
                  <Route exact path="/" element={<CreateStudent/>}/>
                  <Route exact path="/create-student" element={<CreateStudent/>}/>
                  <Route exact path="/edit-student/:id" element={<EditStudent/>}/>
                  <Route exact path="/student-list" element={<StudentList/>}/>
                </Routes>
              </div>
            </Col>
            <Col md={6}>
              <div className="wrapper">
                <img src={school} alt="pics" title="app-pics" />
              </div>
            </Col>
          </Row>
        </Container>

      </div>
    </Router>
  )

};
export default App;