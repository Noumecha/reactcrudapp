// Component for add new student

// import Modules
import React, { useState } from "react";
import axios from "axios";
import StudentForm from "./StudentForm";

// component code
const CreateStudent = () => {
    const [formValues, setFormValues] = useState({ name: '', email: '', roolno: ''})
    // onSubmit handler
    const onSubmit = studentObject => {
        axios.post('http://localhost:4000/students/create-student', studentObject)
        .then(res => {
            if (res.status === 200)
                alert('Etudiant ajouté avec succès')
            else
                Promise.reject()
        })
        .catch(err => alert("Une erreur innatendu s'est produite"))
    }
    // return part
    return (
        <StudentForm initialValues={formValues}
            onSubmit={onSubmit} enableReinitialize>
                Creer un Etudiant
            </StudentForm>
    )
}
export default CreateStudent;