// import Module
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import StudentForm from './StudentForm';

const EditStudent = (props) => {
    const [formValues, setFormValues] = useState({
        name : "",
        email : "",
        rollno : "",
    });
    const onSubmit = (studentObject) => {
        axios
        .put("http://localhost:4000/students/update-student/" + props.match.params.id, studentObject)
        .then((res)=> {
            if (res.status === 200) {
                alert("Etudiant mis à jour avec succès");
                props.history.push("/student-list");
            }
        })
        .catch((err)=> alert("Une erreur innatendu est survenue!"));
    };
    // load the datas from the server and reinitilize student form
    useEffect(() => {
        axios
        .get("http://localhost:4000/students/update-student/" + props.match.params.id)
        .then((res) => {
            const { name, email, rollno } = res.data;
            setFormValues({ name, email, rollno });
        })
        .catch((err) => console.error(err));
    });
    return (
        <StudentForm
            initialValues={formValues}
            onSubmit={onSubmit}
            enableReinitialize>
                Mise à jour
            </StudentForm>
    )
};
export default EditStudent;