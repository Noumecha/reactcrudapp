let mongoose = require("mongoose"),
    express = require("express"),
    router = express.Router();

// student Moddel
let studentSchema = require("../models/Student");

// CREATE Student
router.post('/create-student', (req, res, next) => {
    studentSchema.create(req.body)
    .then((res) => {
        //res.json(req.body)
        console.log(res)
    })
    .catch((err) => {
        return next(err)
    });
    /*studentSchema.create(req.body, (error, data) => {
        if (error) {
            return next(error);
        } else {
            console.log(data);
            res.json(data);
        }
    });*/
});

// READ Students
router.get("/student-list", (req, res, next) => {
    studentSchema.find()
    .then((res) => {
        console.log(req.body);
    })
    .catch((err) => {
        return next(err)
    });
    /*studentSchema.find((error, data) => {
        if (error) {
            return next(error);
        } else {
            res.json(data);
        }
    });*/
});

// UPDATE student
router
.route("/update-student/:id")
// GEt Single Student
.get((req, res, next) => {
    studentSchema.findById(req.params.id, (error, data) => {
        if (error) {
            return next(error);
        } else {
            res.json(data);
        }
    });
})

// Update Student Data
.put((req, res, next) => {
studentSchema.fintByIdAndUpdate(
        req.params.id,
        {
            $set: req.body,
        },
        (error, data) => {
            if (error) {
                return next(error);
                console.error(error);
            } else {
                res.json(data);
                console.log("Etudiant mis à jour avec succès");
            }
        }
    );
});
// Delete Student
router.delete("/student-delete/:id", (req, res, next) => {
    studentSchema.findByIdAndRemove(
        req.params.id, (error, data) => {
            if (error) {
                return next(error);
            } else {
                res.status(200).json({
                    msg : data,
                });
            }
        }
    );
});

module.exports = router;